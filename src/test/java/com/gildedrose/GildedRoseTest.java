package com.gildedrose;


import com.gildedrose.item.Item;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GildedRoseTest {

    @Test
    public void foo() {
        Item[] items = new Item[] { new Item("Elixir of the Mongoose", 0, 0) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals("Elixir of the Mongoose", app.items[0].name);
    }

    @Test
    public void passed_sell_by_date_degrade_quality_twice_test() {
        Item[] items = new Item[] { new Item("Elixir of the Mongoose", 0, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(8, app.items[0].quality);
    }

    @Test
    public void quality_never_negative_test() {
        Item[] items = new Item[] { new Item("Elixir of the Mongoose", 0, 0) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertFalse(app.items[0].quality < 0);
    }

    @Test
    public void aged_brie_increase_in_quality_test() {
        Item[] items = new Item[] { new Item("Aged Brie", 1, 5) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(6, app.items[0].quality);
    }

    @Test
    public void quality_never_more_than_fifty_test() {
        Item[] items = new Item[] { new Item("Aged Brie", 1, 50) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(50, app.items[0].quality);
    }

    @Test
    public void sulfuras_never_decreases_quality_test() {
        Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 1, 80) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(app.items[0].quality, 80);
        assertEquals(app.items[0].sellIn, 1);
    }

    @Test
    public void backstage_passes_increases_quality_by_two_as_sellIn_approaches_test() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 10, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(app.items[0].quality, 12);
    }

    @Test
    public void backstage_passes_increases_quality_by_three_as_sellIn_approaches_test() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 5, 5) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(app.items[0].quality, 8);
    }

    @Test
    public void backstage_passes_quality_drops_to_zero_after_concert_test() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 0, 5) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(app.items[0].quality, 0);
    }

    @Test
    public void conjured_item_degrade_quality_twice_test() {
        Item[] items = new Item[] { new Item("Conjured Mana Cake", 5, 5) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(app.items[0].quality, 3);
    }

}
