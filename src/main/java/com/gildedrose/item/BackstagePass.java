package com.gildedrose.item;

public class BackstagePass extends GenericItem {

    public BackstagePass(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateQuality() {
        if (quality < QUALITY_THRESHOLD) {
            quality = quality + 1;

            if (sellIn < 11) {
                if (quality < QUALITY_THRESHOLD) {
                    quality = quality + 1;
                }
            }

            if (sellIn < 6) {
                if (quality < QUALITY_THRESHOLD) {
                    quality = quality + 1;
                }
            }
        }
        if (sellIn < 0) {
            quality = 0;
        }
    }
}
