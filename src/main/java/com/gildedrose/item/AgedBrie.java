package com.gildedrose.item;

public class AgedBrie extends GenericItem {

    public AgedBrie(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateQuality() {
        if (quality < QUALITY_THRESHOLD)
            quality = quality + 1;

        if (sellIn < 0) {
            if (quality < QUALITY_THRESHOLD) {
                quality = quality + 1;
            }
        }
    }
}
