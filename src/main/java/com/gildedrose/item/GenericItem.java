package com.gildedrose.item;

public class GenericItem extends Item {
    public static final int QUALITY_THRESHOLD = 50;

    public GenericItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    public void updateItem() {
        updateSellIn();
        updateQuality();
    }

    public void updateSellIn() {
        sellIn = sellIn - 1;
    }

    public void updateQuality() {
        if (sellIn < 0 && quality > 1)
            quality = quality - 2;
        else if (quality > 0)
            quality = quality - 1;
    }
}
