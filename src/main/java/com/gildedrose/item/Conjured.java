package com.gildedrose.item;

public class Conjured extends GenericItem {

    public Conjured(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateQuality() {
      if (quality > 1)
            quality = quality - 2;
      else
          quality = 0;
    }
}
