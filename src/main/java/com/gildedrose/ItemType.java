package com.gildedrose;

public enum ItemType {
    AGED_BRIE("Aged Brie"),
    BACKSTAGE_PASSED_TO_A_TAFKAL80ETC_CONCERT("Backstage passes to a TAFKAL80ETC concert"),
    CONJURED("Conjured"),
    SULFURAS("Sulfuras, Hand of Ragnaros"),
    DEFAULT_ITEM("");

    private final String itemName;

    ItemType(String itemName) {
        this.itemName = itemName;
    }

    public static ItemType findByItemName(String itemName){
        for(ItemType itemType : values()){
            if(itemName.toLowerCase().startsWith(itemType.getItemName().toLowerCase())){
                return itemType;
            }
        }
        return DEFAULT_ITEM;
    }

    public String getItemName() {
        return itemName;
    }

}
