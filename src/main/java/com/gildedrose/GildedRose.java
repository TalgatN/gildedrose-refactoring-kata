package com.gildedrose;

import com.gildedrose.item.*;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {
            ItemType itemType = ItemType.findByItemName(item.name);

            switch (itemType) {
                case AGED_BRIE:
                    setItemFields(item, new AgedBrie(item.name, item.sellIn, item.quality));
                    break;
                case BACKSTAGE_PASSED_TO_A_TAFKAL80ETC_CONCERT:
                    setItemFields(item, new BackstagePass(item.name, item.sellIn, item.quality));
                    break;
                case CONJURED:
                    setItemFields(item, new Conjured(item.name, item.sellIn, item.quality));
                    break;
                case SULFURAS:
                    break;
                default:
                    setItemFields(item, new GenericItem(item.name, item.sellIn, item.quality));
                    break;
            }
        }
    }

    private void setItemFields(Item item, GenericItem genericItem) {
        genericItem.updateItem();
        item.name = genericItem.name;
        item.sellIn = genericItem.sellIn;
        item.quality = genericItem.quality;
    }
}